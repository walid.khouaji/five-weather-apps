import React from 'react';
import './styles.css';

const dateObj = new Date();
const days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];

const Weather = ({ weatherData }) => (
  <div className="main">
    <div className="top">
      <p className="header">{weatherData.name}</p>
    </div>
    <div className="flex">
      <p className="day">{days[dateObj.getDay()]}, <span>{dateObj.getDate() + ' ' + dateObj.toLocaleString('en-us', { month: "short" }) + ' ' + dateObj.getFullYear()}</span></p>
      <p className="description">{weatherData.weather[0].main}</p>
    </div>

    <div className="flex">
      <p className="temp">Temperature: {weatherData.main.temp} &deg;C</p>
      <p className="temp">Humidity: {weatherData.main.humidity} %</p>
    </div>

    <div className="flex">
      <p className="sunrise-sunset">Sunrise: {new Date(weatherData.sys.sunrise * 1000).toLocaleTimeString('fr-FR')}</p>
      <p className="sunrise-sunset">Sunset: {new Date(weatherData.sys.sunset * 1000).toLocaleTimeString('fr-FR')}</p>
    </div>

  </div>
)

export default Weather;