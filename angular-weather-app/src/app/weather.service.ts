import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

const apiKey: string = environment.apiKey;
const apiUrl: string = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})

export class WeatherService {
  constructor(private http: HttpClient) { }
  getCurrentWeather(lat: string, lon: string) {
    return this.http.get(`${apiUrl}/weather/?lat=${lat}&lon=${lon}&units=metric&&appid=${apiKey}`)
  }
}