import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Store, select } from '@ngrx/store';
import { WeatherService } from '../weather.service';

@Component({
  selector: 'app-current-weather',
  templateUrl: './current-weather.component.html',
  styleUrls: ['./current-weather.component.css']
})
export class CurrentWeatherComponent implements OnInit {
  dateObj: Date = new Date();
  days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
  lat: string = '';
  lon: string = '';
  currentWeather: any = <any>{};
  msg: string = '';
  dayOfTheWeek: string = this.days[ this.dateObj.getDay() ];
  date: string = this.dateObj.getDate() + ' ' + this.dateObj.toLocaleString('en-us', { month: "short" }) + ' ' + this.dateObj.getFullYear()
  sunrise: string = '';
  sunset: string = '';

  constructor(
    private store: Store<any>,
    private weatherService: WeatherService
  ) {
    navigator.geolocation.getCurrentPosition((position) => {
      this.lat = position.coords.latitude.toString();
      this.lon = position.coords.longitude.toString();
      this.searchWeather(this.lat, this.lon);
    })
  }
  ngOnInit() {
  }
  searchWeather(lat: string, lon: string) {
    this.msg = '';
    this.currentWeather = {};
    this.weatherService.getCurrentWeather(lat, lon)
      .subscribe(res => {
        console.log(res);
        this.currentWeather = res;
        this.sunrise =  new Date(this.currentWeather.sys.sunrise * 1000).toLocaleTimeString('fr-FR');
        this.sunset =  new Date(this.currentWeather.sys.sunset * 1000).toLocaleTimeString('fr-FR');
      }, err => {
        if (err.error && err.error.message) {
          alert(err.error.message);
          this.msg = err.error.message;
          return;
        }
        alert('Failed to get weather.');
      }, () => {
      })
  }
  resultFound() {
    return Object.keys(this.currentWeather).length > 0;
  }
}