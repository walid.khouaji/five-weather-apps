(() => {
  const appid_openweather = "ac0fdd38df429674be21355a4850114b";
  const dateObj = new Date();
  const days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
  const dayOfTheWeek = days[dateObj.getDay()];
  
  navigator.geolocation.getCurrentPosition((position) => {
    let lat = position.coords.latitude;
    let lon = position.coords.longitude;

    fetchWeather(lat, lon)
  });

  const fetchWeather = async (lat, lon) => {
    await fetch(`http://api.openweathermap.org/data/2.5/weather/?lat=${lat}&lon=${lon}&units=metric&APPID=${appid_openweather}`)
      .then(res => res.json())
      .then(result => {
        loading = false;
        document.getElementById("weatherName").innerHTML = result.name;
        document.getElementById("weatherDay").innerHTML = dayOfTheWeek + ", ";
        document.getElementById("weatherDesc").innerHTML = result.weather[0].main;
        document.getElementById("weatherTemp").innerHTML = "Temperature: " + result.main.temp + "&deg;C";
        document.getElementById("weatherHum").innerHTML = "Humidity: " + result.main.humidity + "%";
        document.getElementById("sunset").innerHTML = "Sunset: " + new Date(result.sys.sunset * 1000).toLocaleTimeString('fr-FR');
        document.getElementById("sunrise").innerHTML = "Sunrise: " + new Date(result.sys.sunrise * 1000).toLocaleTimeString('fr-FR');
      })
  }
})(window)
